<?php
require_once 'connection.php'; // подключаем скрипт

$link = mysqli_connect($host, $user, $password, $database) 
	or die("Ошибка " . mysqli_error($link));

$sql = "CREATE TABLE room (
	room_number int(5) auto_increment,
	discipline varchar(30),
	places int(3),
	primary key(room_number)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#первая
$sql = "CREATE TABLE subject (
	subject_name varchar(30) not null,
	learning_programm text,
	hours int(4),
	primary key(subject_name)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#вторая
$sql = "CREATE TABLE teacher (
	teacher_fio varchar(50) not null,
	class_lead varchar(4),
	own_room int(5),
	primary key(teacher_fio),
	foreign key(own_room) references room (room_number)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#восьмая
$sql = "CREATE TABLE specialisation(
	specialisation varchar(30),
	major varchar(60),
	olympiads text,
	primary key(specialisation)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#третья
$sql = "CREATE TABLE class (
	class_number varchar(4) not null,
	specialisation varchar(30),
	primary key(class_number),
	foreign key(specialisation) references specialisation (specialisation)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#четвертая
$sql = "CREATE TABLE student (
	id_student int(5) not null,
	student_fio varchar(50) not null,
    gender char(1),
    stud_class varchar(4) not null,
    primary key(id_student),
    foreign key(stud_class) references class (class_number)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#пятая
$sql = "CREATE TABLE timetable (
	teacher_fio varchar(60) not null,
	room_number int(5) not null,
	class_number varchar(4) not null,
	subject_name varchar(30) not null,
    day varchar(25),
    period varchar(25),
    primary key(class_number),
    foreign key(teacher_fio) references teacher (teacher_fio),
    foreign key(room_number) references room (room_number),
    foreign key(class_number) references class (class_number),
    foreign key(subject_name) references subject (subject_name)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#шестая
$sql = "CREATE TABLE semester_grades (
    id_student int(5) not null,
	stud_class varchar(4) not null,
	subject_name varchar(30) not null,
    value int,
    primary key(id_student),
    foreign key(id_student) references student (id_student),
    foreign key(stud_class) references class (class_number),
    foreign key(subject_name) references subject (subject_name)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}
#седьмая
$sql = "CREATE TABLE parent (
	id_student int(5) not null,
	parent_fio varchar(60) not null,
    phone varchar(15), 
    e_mail varchar(45),
    primary key(parent_fio),
    foreign key(id_student) references student (id_student)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

#ввод данных
$query = "INSERT INTO room VALUES (101,'physics',30), (102,'chemistry',28), (113,'math',46), (114,'biology',30),(204,'russian_language',32)";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO subject VALUES ('algebra',NULL,4), ('biology',NULL,1),('chemistry','federal',20),
    ('geometry',NULL,2), ('physics','individual',5), ('russian_language', 'federal', 3);";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO teacher VALUES ('Babaeva Svetlana Yakovlevna','9-1',102), ('Martemyanova Tatyana Ivanovna',NULL,101),
    ('Serdakova Nataliya Alexandrovna','11-1',204), ('Shirokov Anton Andreevich',NULL,NULL),
    ('Torskiy Evgeniy Dmitrievich','11-2',113);";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO specialisation VALUES ('bio-chemistry','biology,chemistry',NULL), ('rus-lit','languages,literature',NULL),
    ('info-math','informatics, math',NULL), ('phys-math','physics,math',NULL), ('economics','social studies,history',NULL)";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO class VALUES ('9-1','rus-lit'),('9-2','phys-math'),('10-1','info-math'),('10-2','bio-chemistry'),('11-1','phys-math'),('11-2','economics');";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO student VALUES (1,'Chernov Valeriy','m','10-2'), (2,'Ilyina Elena','f','11-2'),(3,'Ivanov Denis','m','11-1'),
    (4,'Korobova Polina','f','10-1'),(5,'Korsukov Maxim','m','10-2'),(6,'Smirnov Dmitriy','m','9-1'),(7,'Zavarin Evgeniy','m','11-1');";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO timetable VALUES ('Martemyanova Tatyana Ivanovna',101,'9-1','physics','friday','9:00 - 9:45'),
    ('Torskiy Evgeniy Dmitrievich',113,'10-1','algebra','tuesday','9:55 - 10:40'),
    ('Martemyanova Tatyana Ivanovna',101,'10-2','physics','wednesday','9:00 - 9:45'),
    ('Serdakova Nataliya Alexandrovna',204,'11-2','russian_language','wednesday','9:55 - 10:40'),
    ('Shirokov Anton Andreevich',113,'9-2','geometry','wednesday','12:05 - 12:50');";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO semester_grades VALUES (1,'10-2','algebra',5),
    (2,'11-2','physics',5),
    (4,'10-1','russian_language',3),
    (5,'10-2','algebra',3)";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}

$query = "INSERT INTO parent VALUES (7,'Zavarin Igor Vasilievich','8-940-876-54-32','igor@gmail.com'),
    (3,'Ivanov Mikhail Fedorovich','8-911-11-11','ivm@mail.ru'),
    (3,'Ivanova Anastasiya Dmitrievna','8-902-202-22-22','nastya@gmail.com'),
    (2,'Ilyin Viktor Alexandrovich','+7-912-123-45-67','vikal@yandex.ru'),
    (7,'Petrova Maria Konstantinovna','+7-911-556-65-56','marko@yandex.ru')";
if (mysqli_query($link, $query)) {
	echo "Inserted";
} else {
	echo "Error insert data: " . mysqli_error($link);
}
 
mysqli_close($link);
?>